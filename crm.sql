CREATE DATABASE crm;
USE crm;
	
CREATE TABLE LoaiThanhVien(
	id INT auto_increment,
	ten VARCHAR(255),
	mota VARCHAR(255),
	primary key(id)
);
CREATE TABLE NguoiDung (
	id INT auto_increment,
	email VARCHAR(255),
	matkhau VARCHAR(255),
	fullname VARCHAR(255),
	diachi VARCHAR(255),
	soDienThoai VARCHAR(255),
	id_loaithanhvien INT;

	primary key(id)
);
CREATE TABLE DuAn(
	id INT AUTO_INCREMENT,
	ten VARCHAR(255),
	moTa VARCHAR(255),
	ngayBatDau DATE,
	ngayKetThuc DATE,
	id_TrangThai INT, 
	id_NguoiQuanLy INT,
	PRIMARY KEY(id)
);
CREATE TABLE DuAn_NguoiDung(
	id_DuAn INT,
	id_NguoiDung INT,
	PRIMARY KEY(id_DuAn, id_NguoiDung)
);

CREATE TABLE CongViec(
	id INT AUTO_INCREMENT,
	ten VARCHAR(20),
	moTa TEXT,
	ngayBatDau DATE,
	ngayKetThuc DATE,
	id_TrangThai INT,
	id_DuAn INT,
	PRIMARY KEY(id)
	
);
CREATE TABLE CongViec_NguoiDung(
	id_CongViec INT,
	id_NguoiDung INT,
	PRIMARY KEY(id_CongViec, id_NguoiDung)
);
CREATE TABLE TrangThai(
	id INT AUTO_INCREMENT,
	ten VARCHAR(255),
	moTa VARCHAR(255),
	PRIMARY KEY(id)
);


ALTER TABLE NguoiDung ADD CONSTRAINT FK_id_loaithanhvien_NguoiDung
FOREIGN KEY(id_loaithanhvien) REFERENCES LoaiThanhVien(id);

ALTER TABLE DuAn_NguoiDung ADD CONSTRAINT FK_id_DuAn_DuAn_NguoiDung 
FOREIGN KEY (id_DuAn) REFERENCES DuAn(id);

ALTER TABLE DuAn_NguoiDung ADD CONSTRAINT FK_id_NguoiDung_DuAn_NguoiDung 
FOREIGN KEY (id_NguoiDung) REFERENCES NguoiDung(id);
====
ALTER TABLE CongViec ADD CONSTRAINT FK_id_TrangThai_CongViec
FOREIGN KEY (id_TrangThai) REFERENCES TrangThai(id);

ALTER TABLE CongViec ADD CONSTRAINT FK_id_DuAn_CongViec
FOREIGN KEY (id_DuAn) REFERENCES DuAn(id);

ALTER TABLE CongViec_NguoiDung ADD CONSTRAINT FK_id_CongViec_CongViec_NguoiDung
FOREIGN KEY(id_CongViec) REFERENCES CongViec(id);

ALTER TABLE CongViec_NguoiDung ADD CONSTRAINT FK_id_NguoiDung_CongViec_NguoiDung
FOREIGN KEY(id_NguoiDung) REFERENCES NguoiDung(id);

ALTER TABLE DuAn ADD CONSTRAINT FK_id_TrangThai_DuAn 
FOREIGN KEY(id_TrangThai) REFERENCES TrangThai(id);

ALTER TABLE DuAn ADD CONSTRAINT FK_id_NguoiQuanLy_DuAn 
FOREIGN KEY(id_NguoiQuanLy) REFERENCES NguoiDung(id);